import { v4 } from 'uuid';

export const ANSWER_CORRECT = 'ANSWER_CORRECT';
export const ANSWER_WRONG = 'ANSWER_WRONG';
export const CREATE_CARD = 'CREATE_CARD';
export const EDIT_CARD = 'EDIT_CARD';
export const LOAD_CARDS_STATE = 'LOAD_CARDS_STATE';
export const REMOVE_CARD = 'REMOVE_CARD';

export const answerCorrect = id => ({
  type: ANSWER_CORRECT,
  id,
});

export const answerWrong = id => ({
  type: ANSWER_WRONG,
  id,
});

export const createCard = (front, back, id = v4()) => ({
  type: CREATE_CARD,
  front,
  back,
  review: Date.now(),
  streak: 0,
  id,
});

export const editCard = (id, front, back, checked) => ({
  type: EDIT_CARD,
  id,
  front,
  back,
  checked,
});

export const loadCardsState = state => ({
  type: LOAD_CARDS_STATE,
  state,
});

export const removeCard = id => ({
  type: REMOVE_CARD,
  id,
});
