import { v4 } from 'uuid';

export const ADD_CARD_TO_DECK = 'ADD_CARD_TO_DECK';
export const CHANGE_DECK_NAME = 'CHANGE_DECK_NAME';
export const CREATE_DECK = 'CREATE_DECK';
export const LOAD_DECKS_STATE = 'LOAD_DECKS_STATE';
export const REMOVE_CARD_FROM_DECK = 'REMOVE_CARD_FROM_DECK';
export const REMOVE_DECK = 'REMOVE_DECK';

export const addCardToDeck = (deckId, cardId) => ({
  type: ADD_CARD_TO_DECK,
  deckId,
  cardId,
});

export const changeDeckName = (id, newName) => ({
  type: CHANGE_DECK_NAME,
  id,
  newName,
});

export const createDeck = (name, id = v4()) => ({
  type: CREATE_DECK,
  name,
  id,
});

export const loadDecksState = state => ({
  type: LOAD_DECKS_STATE,
  state,
});

export const removeCardFromDeck = (deckId, cardId) => ({
  type: REMOVE_CARD_FROM_DECK,
  deckId,
  cardId,
});

export const removeDeck = id => ({
  type: REMOVE_DECK,
  id,
});
