import React from 'react';
import {
  Card,
  DataTable,
  TableCardHeader,
  TableColumn,
  TableHeader,
  TableRow,
} from 'react-md';

import AddCardDialog from '../containers/AddCardDialog';
import CardTableBody from '../containers/CardTableBody';

const CardTable = () => (
  <Card tableCard style={{ maxWidth: 600 }} className="md-block-centered">
    <TableCardHeader title="Cards" visible={false}>
      <div />
      <AddCardDialog />
    </TableCardHeader>

    <DataTable baseId="cards">
      <TableHeader>
        <TableRow autoAdjust={false}>
          <TableColumn>Front</TableColumn>
          <TableColumn>Back</TableColumn>
          <TableColumn>Streak</TableColumn>
          <TableColumn>Review</TableColumn>
          <TableColumn>Actions</TableColumn>
        </TableRow>
      </TableHeader>
      <CardTableBody />
    </DataTable>
  </Card>
);

export default CardTable;
