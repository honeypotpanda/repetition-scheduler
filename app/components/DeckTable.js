import React from 'react';
import {
  Card,
  DataTable,
  TableCardHeader,
} from 'react-md';

import CreateDeckDialog from '../containers/CreateDeckDialog';
import DeckTableHeader from './DeckTableHeader';
import DeckTableBody from '../containers/DeckTableBody';

const DeckTable = () => (
  <Card tableCard style={{ maxWidth: 600 }} className="md-block-centered">
    <TableCardHeader title="Decks" visible={false}>
      <div />
      <CreateDeckDialog />
    </TableCardHeader>

    <DataTable baseId="decks">
      <DeckTableHeader />
      <DeckTableBody />
    </DataTable>
  </Card>
);

export default DeckTable;
