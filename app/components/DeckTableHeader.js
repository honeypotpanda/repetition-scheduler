import React from 'react';
import { TableColumn, TableHeader, TableRow } from 'react-md';

const cn = 'md-table-column--adjusted';

const DeckTableHeader = () => (
  <TableHeader>
    <TableRow autoAdjust={false}>
      <TableColumn>Name</TableColumn>
      <TableColumn numeric className={cn}>Cards</TableColumn>
      <TableColumn numeric className={cn}>Review Cards</TableColumn>
      <TableColumn className={cn}>Actions</TableColumn>
    </TableRow>
  </TableHeader>
);

export default DeckTableHeader;
