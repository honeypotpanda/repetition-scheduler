import React from 'react';
import { Link } from 'react-router';
import { Button, Toolbar } from 'react-md';

const NavBar = () => (
  <div className="toolbar-group">
    <Toolbar
      nav={<Link to="/"><Button key="home" icon>home</Button></Link>}
    />
  </div>
);

export default NavBar;
