import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, TextField } from 'react-md';

import { createCard } from '../actions/card';
import { addCardToDeck } from '../actions/deck';

const propTypes = {
  addCardToDeck: PropTypes.func.isRequired,
  createCard: PropTypes.func.isRequired,
  deckId: PropTypes.string.isRequired,
};

class AddCardDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = { front: '', back: '', error: false, visible: false };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { deckId } = this.props;
    const { front, back } = this.state;

    if (!front || !back) {
      this.setState({ error: true });
    } else {
      const action = this.props.createCard(front, back);
      this.props.addCardToDeck(deckId, action.id);
      this.closeDialog();
    }
  }

  handleFrontChange = (front) => {
    this.setState({ front });
  }

  handleBackChange = (back) => {
    this.setState({ back });
  }

  openDialog = () => {
    this.setState({ visible: true });
  }

  closeDialog = () => {
    this.setState({ front: '', back: '', error: false, visible: false });
  }

  render() {
    const { error } = this.state;

    return (
      <div>
        <Button raised primary label="Add" onClick={this.openDialog} />
        <Dialog
          id="addCardDialog"
          visible={this.state.visible}
          title="New Card"
          onHide={this.closeDialog}
        >
          <form onSubmit={this.handleSubmit}>
            <TextField
              id="front"
              label="Front"
              onChange={this.handleFrontChange}
              type="text"
              error={error}
              required
            />
            <TextField
              id="back"
              label="Back"
              onChange={this.handleBackChange}
              type="text"
              error={error}
              required
            />
            <Button raised primary label="Add" type="submit" />
            <Button raised label="Cancel" onClick={this.closeDialog} />
          </form>
        </Dialog>
      </div>
    );
  }
}

AddCardDialog.propTypes = propTypes;

const mapStateToProps = state => ({
  deckId: state.routing.locationBeforeTransitions.pathname.substring(7, 43),
});

const mapDispatchToProps = dispatch => ({
  addCardToDeck: (deckId, cardId) => dispatch(addCardToDeck(deckId, cardId)),
  createCard: (front, back) => dispatch(createCard(front, back)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddCardDialog);
