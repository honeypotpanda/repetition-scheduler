import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import jsonfile from 'jsonfile';

import { loadCardsState } from '../actions/card';
import { loadDecksState } from '../actions/deck';
import NavBar from '../components/NavBar';

const propTypes = {
  children: PropTypes.element.isRequired,
  loadCardsState: PropTypes.func.isRequired,
  loadDecksState: PropTypes.func.isRequired,
};

class App extends Component {
  componentDidMount() {
    jsonfile.readFile('./app/data.json', (err, data) => {
      if (err) {
        return console.error(err);
      }
      this.props.loadCardsState(data.cards);
      this.props.loadDecksState(data.decks);
    });
  }

  render() {
    return (
      <div>
        <NavBar />
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = propTypes;

const mapDispatchToProps = dispatch => ({
  loadCardsState: cards => dispatch(loadCardsState(cards)),
  loadDecksState: decks => dispatch(loadDecksState(decks)),
});

export default connect(
  null,
  mapDispatchToProps
)(App);
