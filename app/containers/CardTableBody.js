import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  TableColumn,
  TableBody,
  TableRow,
} from 'react-md';
import moment from 'moment';

import EditCardDialog from './EditCardDialog';
import RemoveCardButton from './RemoveCardButton';

const propTypes = {
  cardsById: PropTypes.object.isRequired,
  deckId: PropTypes.string.isRequired,
  decksById: PropTypes.object.isRequired,
};

class CardTableBody extends Component {
  render() {
    const { cardsById, deckId, decksById } = this.props;
    const cards = decksById[deckId].cards.map(cardId => cardsById[cardId]);

    return (
      <TableBody>
        {cards.map(card =>
          <TableRow key={card.id}>
            <TableColumn>{card.front}</TableColumn>
            <TableColumn>{card.back}</TableColumn>
            <TableColumn>{card.streak}</TableColumn>
            <TableColumn>
              {moment(card.review).format('MM/DD/YYYY HH:mm:ss')}
            </TableColumn>
            <TableColumn>
              <EditCardDialog id={card.id} />
              <RemoveCardButton deckId={deckId} cardId={card.id} />
            </TableColumn>
          </TableRow>
        )}
      </TableBody>
    );
  }
}

CardTableBody.propTypes = propTypes;

const mapStateToProps = state => ({
  cardsById: state.cards.byId,
  deckId: state.routing.locationBeforeTransitions.pathname.substring(7, 43),
  decksById: state.decks.byId,
});

export default connect(mapStateToProps)(CardTableBody);
