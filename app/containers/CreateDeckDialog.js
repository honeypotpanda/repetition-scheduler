import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, TextField } from 'react-md';

import { createDeck } from '../actions/deck';

const propTypes = {
  createDeck: PropTypes.func.isRequired,
};

class CreateDeckDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '', error: false, visible: false };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { name } = this.state;

    if (!name) {
      this.setState({ error: true });
    } else {
      this.props.createDeck(name);
      this.closeDialog();
    }
  }

  handleChange = (name) => {
    this.setState({ name });
  }

  openDialog = () => {
    this.setState({ visible: true });
  }

  closeDialog = () => {
    this.setState({ name: '', error: false, visible: false });
  }

  render() {
    const { error } = this.state;

    return (
      <div>
        <Button raised primary label="Create" onClick={this.openDialog} />
        <Dialog
          id="createDeckDialog"
          visible={this.state.visible}
          title="New Deck"
          onHide={this.closeDialog}
        >
          <form onSubmit={this.handleSubmit}>
            <TextField
              id="deckName"
              label="Name"
              onChange={this.handleChange}
              type="text"
              error={error}
              required
            />
            <Button raised primary label="Create" type="submit" />
            <Button raised label="Cancel" onClick={this.closeDialog} />
          </form>
        </Dialog>
      </div>
    );
  }
}

CreateDeckDialog.propTypes = propTypes;

const mapDispatchToProps = dispatch => ({
  createDeck: name => dispatch(createDeck(name)),
});

export default connect(
  null,
  mapDispatchToProps
)(CreateDeckDialog);
