import React, { Component, PropTypes } from 'react';
import { Button, TableBody, TableColumn, TableRow } from 'react-md';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import EditDeckDialog from './EditDeckDialog';
import RemoveDeckButton from './RemoveDeckButton';

const cn = 'md-table-column--adjusted';

const propTypes = {
  cardsById: PropTypes.object.isRequired,
  decksById: PropTypes.object.isRequired,
  deckIds: PropTypes.array.isRequired,
};

class DeckTableBody extends Component {
  countReviewCards = (cardIds) => {
    const { cardsById } = this.props;
    const cards = cardIds.map(cardId => cardsById[cardId]);

    const countReviewCardsHelper = (reviewCards, card) => {
      if (card.review <= Date.now()) {
        return reviewCards + 1;
      }
      return reviewCards;
    };

    return cards.reduce(countReviewCardsHelper, 0);
  }

  render() {
    const { decksById, deckIds } = this.props;
    const decks = deckIds.map(id => decksById[id]);

    return (
      <TableBody>
        {decks.map(deck =>
          <TableRow key={deck.id}>
            <TableColumn><Link to={`/decks/${deck.id}/cards`}>{deck.name}</Link></TableColumn>
            <TableColumn className={cn} numeric>
              {deck.cards.length}
            </TableColumn>
            <TableColumn className={cn} numeric>
              {this.countReviewCards(deck.cards)}
            </TableColumn>
            <TableColumn>
              <EditDeckDialog id={deck.id} />
              <RemoveDeckButton id={deck.id} cardIds={deck.cards} />
              <Link to={`/decks/${deck.id}/review`}><Button icon>book</Button></Link>
            </TableColumn>
          </TableRow>
        )}
      </TableBody>
    );
  }
}

DeckTableBody.propTypes = propTypes;

const mapStateToProps = state => ({
  cardsById: state.cards.byId,
  decksById: state.decks.byId,
  deckIds: state.decks.allIds,
});

export default connect(mapStateToProps)(DeckTableBody);
