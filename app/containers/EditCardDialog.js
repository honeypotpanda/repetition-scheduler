import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Checkbox, Dialog, TextField } from 'react-md';

import { editCard } from '../actions/card';

const propTypes = {
  editCard: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
};

class EditCardDialog extends Component {
  state = {
    front: '',
    back: '',
    checked: false,
    frontError: false,
    backError: false,
    visible: false,
  };

  handleFrontChange = front => this.setState({ front });
  handleBackChange = back => this.setState({ back });
  handleCheckedChange = checked => this.setState({ checked });

  handleSubmit = (e) => {
    e.preventDefault();

    this.setState({ frontError: false, backError: false });

    const { id } = this.props;
    const { front, back, checked } = this.state;

    if (!front || !back) {
      if (!front) {
        this.setState({ frontError: true });
      }

      if (!back) {
        this.setState({ backError: true });
      }
    } else {
      this.props.editCard(id, front, back, checked);
      this.closeDialog();
    }
  }

  handleChange = (name) => {
    this.setState({ name });
  }

  openDialog = () => {
    this.setState({ visible: true });
  }

  closeDialog = () => {
    this.setState({
      front: '',
      back: '',
      checked: false,
      frontError: false,
      backError: false,
      visible: false,
    });
  }

  render() {
    const { front, back, frontError, backError, visible } = this.state;

    return (
      <span>
        <Button icon onClick={this.openDialog}>edit</Button>
        <Dialog
          id="editCardDialog"
          visible={visible}
          title="Edit Card"
          onHide={this.closeDialog}
        >
          <form onSubmit={this.handleSubmit}>
            <TextField
              id="front"
              label="Front"
              onChange={this.handleFrontChange}
              type="text"
              error={frontError}
              required
              value={front}
            />
            <TextField
              id="back"
              label="Back"
              onChange={this.handleBackChange}
              type="text"
              error={backError}
              required
              value={back}
            />
            <Checkbox
              id="resetStreak"
              name="resetStreak"
              label="Reset streak"
              onChange={this.handleCheckedChange}
            />
            <Button raised primary label="Save" type="submit" />
            <Button raised label="Cancel" onClick={this.closeDialog} />
          </form>
        </Dialog>
      </span>
    );
  }
}

EditCardDialog.propTypes = propTypes;

const mapDispatchToProps = dispatch => ({
  editCard: (id, front, back, checked) => dispatch(editCard(id, front, back, checked)),
});

export default connect(
  null,
  mapDispatchToProps
)(EditCardDialog);
