import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, TextField } from 'react-md';

import { changeDeckName } from '../actions/deck';

const propTypes = {
  changeDeckName: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
};

class EditDeckDialog extends Component {
  constructor(props) {
    super(props);
    this.state = { name: '', error: false, visible: false };
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { id } = this.props;
    const { name } = this.state;

    if (!name) {
      this.setState({ error: true });
    } else {
      this.props.changeDeckName(id, name);
      this.closeDialog();
    }
  }

  handleChange = (name) => {
    this.setState({ name });
  }

  openDialog = () => {
    this.setState({ visible: true });
  }

  closeDialog = () => {
    this.setState({ name: '', error: false, visible: false });
  }

  render() {
    const { error, name, visible } = this.state;

    return (
      <span>
        <Button icon onClick={this.openDialog}>edit</Button>
        <Dialog
          id="changeDeckNameDialog"
          visible={visible}
          title="Edit Deck Name"
          onHide={this.closeDialog}
        >
          <form onSubmit={this.handleSubmit}>
            <TextField
              id="deckName"
              label="Name"
              onChange={this.handleChange}
              type="text"
              error={error}
              required
              value={name}
            />
            <Button raised primary label="Save" type="submit" />
            <Button raised label="Cancel" onClick={this.closeDialog} />
          </form>
        </Dialog>
      </span>
    );
  }
}

EditDeckDialog.propTypes = propTypes;

const mapDispatchToProps = dispatch => ({
  changeDeckName: (id, name) => dispatch(changeDeckName(id, name)),
});

export default connect(
  null,
  mapDispatchToProps
)(EditDeckDialog);
