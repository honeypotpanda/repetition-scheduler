// @flow
import React, { Component } from 'react';

import DeckTable from '../components/DeckTable';

export default class HomePage extends Component {
  render() {
    return (
      <div>
        <DeckTable />
      </div>
    );
  }
}
