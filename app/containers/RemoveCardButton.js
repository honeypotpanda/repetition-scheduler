import React, { Component, PropTypes } from 'react';
import { Button } from 'react-md';
import { connect } from 'react-redux';

import { removeCard } from '../actions/card';
import { removeCardFromDeck } from '../actions/deck';

const propTypes = {
  deckId: PropTypes.string.isRequired,
  cardId: PropTypes.string.isRequired,
  removeCard: PropTypes.func.isRequired,
  removeCardFromDeck: PropTypes.func.isRequired,
};

class RemoveCardButton extends Component {
  handleClick = () => {
    const { deckId, cardId } = this.props;
    this.props.removeCardFromDeck(deckId, cardId);
    this.props.removeCard(cardId);
  }

  render() {
    return (
      <Button icon onClick={this.handleClick}>
        delete
      </Button>
    );
  }
}

RemoveCardButton.propTypes = propTypes;

const mapDispatchToProps = dispatch => ({
  removeCard: id => dispatch(removeCard(id)),
  removeCardFromDeck: (deckId, cardId) => dispatch(removeCardFromDeck(deckId, cardId)),
});

export default connect(
  null,
  mapDispatchToProps
)(RemoveCardButton);
