import React, { Component, PropTypes } from 'react';
import { Button } from 'react-md';
import { connect } from 'react-redux';

import { removeCard } from '../actions/card';
import { removeDeck } from '../actions/deck';

const propTypes = {
  id: PropTypes.string.isRequired,
  cardIds: PropTypes.array.isRequired,
  removeCard: PropTypes.func.isRequired,
  removeDeck: PropTypes.func.isRequired,
};

class RemoveDeckButton extends Component {
  handleClick = () => {
    const { id, cardIds } = this.props;
    this.props.removeDeck(id);
    cardIds.forEach(cardId => this.props.removeCard(cardId));
  };

  render() {
    return (
      <Button icon onClick={this.handleClick}>
        delete
      </Button>
    );
  }
}

RemoveDeckButton.propTypes = propTypes;

const mapDispatchToProps = dispatch => ({
  removeCard: id => dispatch(removeCard(id)),
  removeDeck: id => dispatch(removeDeck(id)),
});

export default connect(
  null,
  mapDispatchToProps
)(RemoveDeckButton);
