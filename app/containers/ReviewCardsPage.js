import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Card,
  CardTitle,
  CardActions,
} from 'react-md';

import { answerCorrect, answerWrong } from '../actions/card';

const propTypes = {
  answerCorrect: PropTypes.func.isRequired,
  answerWrong: PropTypes.func.isRequired,
  cardsById: PropTypes.object.isRequired,
  decksById: PropTypes.object.isRequired,
  deckId: PropTypes.string.isRequired,
};

class ReviewCardsPage extends Component {
  state = { visible: false };

  handleVisibilityClick = () => this.setState({ visible: true });

  handleCorrectClick = id => {
    this.props.answerCorrect(id);
    this.setState({ visible: false });
  }

  handleWrongClick = id => {
    this.props.answerWrong(id);
    this.setState({ visible: false });
  }

  render() {
    const { cardsById, decksById, deckId } = this.props;
    const deck = decksById[deckId];
    const cards = deck.cards.map(cardId => cardsById[cardId]);
    const reviewCards = cards.filter(card => card.review <= Date.now());

    if (reviewCards.length === 0) {
      return (
        <Card style={{ maxWidth: 600 }} className="md-block-centered">
          <CardTitle
            title=""
            subtitle="You don't have any cards to review."
          />
        </Card>
      );
    }

    const { visible } = this.state;
    const reviewCard = reviewCards[0];
    const { id, front, back } = reviewCard;

    return (
      <Card style={{ maxWidth: 600 }} className="md-block-centered">
        <CardTitle title="" subtitle={front}>
          <Button
            primary
            icon
            onClick={this.handleVisibilityClick}
            style={{ display: visible ? 'none' : '' }}
          >
            visibility
          </Button>
        </CardTitle>
        <div style={{ display: visible ? '' : 'none' }}>
          <CardTitle title="" subtitle={back} />
          <CardActions>
            <Button primary icon onClick={() => this.handleCorrectClick(id)}>
              done
            </Button>
            <Button secondary icon onClick={() => this.handleWrongClick(id)}>
              clear
            </Button>
          </CardActions>
        </div>
      </Card>
    );
  }
}

ReviewCardsPage.propTypes = propTypes;

const mapStateToProps = state => ({
  cardsById: state.cards.byId,
  decksById: state.decks.byId,
  deckId: state.routing.locationBeforeTransitions.pathname.substring(7, 43),
});

const mapStateToDispatch = dispatch => ({
  answerCorrect: id => dispatch(answerCorrect(id)),
  answerWrong: id => dispatch(answerWrong(id)),
});

export default connect(
  mapStateToProps,
  mapStateToDispatch
)(ReviewCardsPage);
