import jsonfile from 'jsonfile';

jsonfile.spaces = 2;
const file = './app/data.json';

const saveData = (store) => (next) => (action) => {
  const result = next(action);

  if (action.type !== '@@router/LOCATION_CHANGE') {
    const { routing, ...state } = store.getState();
    jsonfile.writeFileSync(file, state, (err) => {
      if (err) {
        console.error(err);
      }
    });
  }

  return result;
};

export default saveData;
