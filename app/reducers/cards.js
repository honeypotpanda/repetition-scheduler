/* eslint no-param-reassign: ["error", { "props": false }] */

import moment from 'moment';

import {
  ANSWER_CORRECT,
  ANSWER_WRONG,
  CREATE_CARD,
  EDIT_CARD,
  LOAD_CARDS_STATE,
  REMOVE_CARD,
} from '../actions/card';

const omit = (obj, omitKey) =>
  Object.keys(obj).reduce((result, key) => {
    if (key !== omitKey) {
      result[key] = obj[key];
    }
    return result;
  }, {});

const initialState = {
  byId: {},
  allIds: [],
};

const cards = (state = initialState, action) => {
  switch (action.type) {
    case ANSWER_CORRECT: {
      const { id } = action;
      const streak = state.byId[id].streak + 1;
      const review = moment(state.byId[id].review)
        .startOf('day')
        .add(streak, 'days')
        .valueOf();

      return {
        byId: {
          ...state.byId,
          [id]: {
            ...state.byId[id],
            review,
            streak,
          },
        },
        allIds: [...state.allIds],
      };
    }
    case ANSWER_WRONG: {
      const { id } = action;
      const streak = 0;
      const review = moment(Date.now())
        .add(5, 'minutes')
        .valueOf();

      return {
        byId: {
          ...state.byId,
          [id]: {
            ...state.byId[id],
            review,
            streak,
          },
        },
        allIds: [...state.allIds],
      };
    }
    case CREATE_CARD: {
      const { id, front, back, review, streak } = action;

      return {
        byId: {
          ...state.byId,
          [id]: {
            id,
            front,
            back,
            review,
            streak,
          },
        },
        allIds: [...state.allIds, id],
      };
    }
    case EDIT_CARD: {
      const { id, front, back, checked } = action;

      if (checked) {
        return {
          byId: {
            ...state.byId,
            [id]: {
              id,
              front,
              back,
              review: Date.now(),
              streak: 0,
            },
          },
          allIds: [...state.allIds],
        };
      }

      return {
        byId: {
          ...state.byId,
          [id]: {
            ...state.byId[id],
            front,
            back,
          },
        },
        allIds: [...state.allIds],
      };
    }
    case LOAD_CARDS_STATE: {
      return action.state;
    }
    case REMOVE_CARD: {
      const { id } = action;

      return {
        byId: omit(state.byId, id),
        allIds: state.allIds.filter(cardId => cardId !== id),
      };
    }
    default: {
      return state;
    }
  }
};

export default cards;
