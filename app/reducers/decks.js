/* eslint no-param-reassign: ["error", { "props": false }] */

import {
  ADD_CARD_TO_DECK,
  CHANGE_DECK_NAME,
  CREATE_DECK,
  LOAD_DECKS_STATE,
  REMOVE_CARD_FROM_DECK,
  REMOVE_DECK,
} from '../actions/deck';

const omit = (obj, omitKey) =>
  Object.keys(obj).reduce((result, key) => {
    if (key !== omitKey) {
      result[key] = obj[key];
    }
    return result;
  }, {});

const initialState = {
  byId: {},
  allIds: [],
};

const decks = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CARD_TO_DECK: {
      const { deckId, cardId } = action;

      return {
        byId: {
          ...state.byId,
          [deckId]: {
            ...state.byId[deckId],
            cards: [...state.byId[deckId].cards, cardId],
          },
        },
        allIds: [...state.allIds],
      };
    }
    case CHANGE_DECK_NAME: {
      const id = action.id;

      return {
        byId: {
          ...state.byId,
          [id]: {
            id,
            name: action.newName,
            cards: state.byId[id].cards,
          },
        },
        allIds: state.allIds,
      };
    }
    case CREATE_DECK: {
      const id = action.id;
      const byId = { ...state.byId };
      byId[id] = {
        id,
        name: action.name,
        cards: [],
      };

      return {
        byId,
        allIds: [...state.allIds, id],
      };
    }
    case LOAD_DECKS_STATE: {
      return action.state;
    }
    case REMOVE_CARD_FROM_DECK: {
      const { deckId, cardId } = action;

      return {
        byId: {
          ...state.byId,
          [deckId]: {
            ...state.byId[deckId],
            cards: state.byId[deckId].cards.filter(id => id !== cardId),
          }
        },
        allIds: state.allIds,
      };
    }
    case REMOVE_DECK: {
      const id = action.id;

      return {
        byId: omit(state.byId, id),
        allIds: state.allIds.filter(elem => elem !== id),
      };
    }
    default: {
      return state;
    }
  }
};

export default decks;
