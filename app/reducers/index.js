// @flow
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import cards from './cards';
import decks from './decks';

const rootReducer = combineReducers({
  routing,
  cards,
  decks,
});

export default rootReducer;
