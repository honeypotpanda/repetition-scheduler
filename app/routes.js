// @flow
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import CardTable from './components/CardTable';
import ReviewCardsPage from './containers/ReviewCardsPage';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="/decks/:deckId/cards" component={CardTable} />
    <Route path="/decks/:deckId/review" component={ReviewCardsPage} />
  </Route>
);
