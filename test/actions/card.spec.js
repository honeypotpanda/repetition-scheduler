/* global describe expect it */

import * as actions from '../../app/actions/card';

describe('card actions', () => {
  it('should create ANSWER_CORRECT action', () => {
    const id = '0';

    expect(actions.answerCorrect(id)).toEqual({
      type: actions.ANSWER_CORRECT,
      id,
    });
  });

  it('should create ANSWER_WRONG action', () => {
    const id = '0';

    expect(actions.answerWrong(id)).toEqual({
      type: actions.ANSWER_WRONG,
      id,
    });
  });

  it('should create CREATE_CARD action', () => {
    const front = 'front 0';
    const back = 'back 0';
    const action = actions.createCard(front, back);

    expect(action.type).toEqual(actions.CREATE_CARD);
    expect(action.front).toEqual(front);
    expect(action.back).toEqual(back);
    expect(action.review).toBeCloseTo(Date.now(), -2);
    expect(action.streak).toEqual(0);
    expect(action).toHaveProperty('id');
  });

  it('should create EDIT_CARD action', () => {
    const id = '0';
    const front = 'front 0';
    const back = 'back 0';
    const checked = false;

    expect(actions.editCard(id, front, back, checked)).toEqual({
      type: actions.EDIT_CARD,
      id,
      front,
      back,
      checked,
    });
  });

  it('should create REMOVE_CARD action', () => {
    const id = '0';
    expect(actions.removeCard(id)).toEqual({
      type: actions.REMOVE_CARD,
      id,
    });
  });

  it('should create LOAD_CARDS_STATE action', () => {
    const state = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 1234,
          streak: 0,
        },
        1: {
          id: '1',
          front: 'front 1',
          back: 'back 1',
          review: 1234,
          streak: 2,
        }
      },
      allIds: ['0', '1'],
    };

    expect(actions.loadCardsState(state)).toEqual({
      type: actions.LOAD_CARDS_STATE,
      state,
    });
  });
});
