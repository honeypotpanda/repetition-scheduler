/* global describe expect it */

import * as actions from '../../app/actions/deck';

describe('deck actions', () => {
  it('should create ADD_CARD_TO_DECK', () => {
    const deckId = '0';
    const cardId = '0';
    expect(actions.addCardToDeck(deckId, cardId)).toEqual({
      type: actions.ADD_CARD_TO_DECK,
      deckId,
      cardId,
    });
  });

  it('should create CHANGE_DECK_NAME action', () => {
    const id = '0';
    const newName = 'Deck 0';

    expect(actions.changeDeckName(id, newName)).toEqual({
      type: actions.CHANGE_DECK_NAME,
      id,
      newName,
    });
  });

  it('should create CREATE_DECK action', () => {
    const action = actions.createDeck('deck');

    expect(action.type).toEqual(actions.CREATE_DECK);
    expect(action.name).toEqual('deck');
    expect(action).toHaveProperty('id');
  });

  it('should create LOAD_DECKS_STATE action', () => {
    const state = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: []
        },
        1: {
          id: '1',
          name: 'Deck 1',
          cards: []
        }
      },
      allIds: ['0', '1'],
    };

    expect(actions.loadDecksState(state)).toEqual({
      type: actions.LOAD_DECKS_STATE,
      state,
    });
  });

  it('should create REMOVE_CARD_FROM_DECK', () => {
    const deckId = '0';
    const cardId = '0';

    expect(actions.removeCardFromDeck(deckId, cardId)).toEqual({
      type: actions.REMOVE_CARD_FROM_DECK,
      deckId,
      cardId,
    });
  });

  it('should create REMOVE_DECK action', () => {
    const id = '0';
    expect(actions.removeDeck(id)).toEqual({
      type: actions.REMOVE_DECK,
      id,
    });
  });
});
