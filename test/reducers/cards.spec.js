/* global describe expect it */

import reducer from '../../app/reducers/cards';
import * as actions from '../../app/actions/card';

describe('cards reducer', () => {
  it('should handle initial state', () => {
    const stateBefore = undefined;
    const action = {};
    const stateAfter = {
      byId: {},
      allIds: [],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('should handle ANSWER_CORRECT action', () => {
    const id = '0';

    const stateBefore = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 1492412400012, // April 17, 2017
          streak: 1,
        },
      },
      allIds: ['0'],
    };
    const action = {
      type: actions.ANSWER_CORRECT,
      id,
    };
    const stateAfter = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 1492585200000, // April 19, 2017
          streak: 2,
        },
      },
      allIds: ['0'],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('should handle ANSWER_WRONG action', () => {
    const id = '0';

    const stateBefore = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 1492412400012,
          streak: 1,
        },
      },
      allIds: ['0'],
    };
    const action = {
      type: actions.ANSWER_WRONG,
      id,
    };
    const stateAfter = reducer(stateBefore, action);

    expect(stateAfter.byId[id].id).toEqual('0');
    expect(stateAfter.byId[id].front).toEqual('front 0');
    expect(stateAfter.byId[id].back).toEqual('back 0');
    expect(stateAfter.byId[id].review).toBeCloseTo(Date.now() + 300000, -1);
    expect(stateAfter.byId[id].streak).toEqual(0);
    expect(stateAfter.allIds).toEqual(['0']);
  });

  it('should handle CREATE_CARD action', () => {
    const front = 'front 1';
    const back = 'back 1';
    const id = '1';

    const stateBefore = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: Date.now(),
          streak: 0,
        },
      },
      allIds: ['0'],
    };
    const action = actions.createCard(front, back, id);
    const stateAfter = reducer(stateBefore, action);

    expect(stateAfter.byId).toHaveProperty('0');
    expect(stateAfter.byId[0].id).toEqual('0');
    expect(stateAfter.byId[0].front).toEqual('front 0');
    expect(stateAfter.byId[0].back).toEqual('back 0');
    expect(stateAfter.byId[0].review).toBeCloseTo(Date.now(), -2);
    expect(stateAfter.byId[0].streak).toEqual(0);
    expect(stateAfter.byId).toHaveProperty('1');
    expect(stateAfter.byId[1].id).toEqual('1');
    expect(stateAfter.byId[1].front).toEqual('front 1');
    expect(stateAfter.byId[1].back).toEqual('back 1');
    expect(stateAfter.byId[1].review).toBeCloseTo(Date.now(), -2);
    expect(stateAfter.byId[1].streak).toEqual(0);
    expect(stateAfter.allIds).toHaveLength(2);
  });

  it('should handle EDIT_CARD with checked', () => {
    const id = '0';
    const front = 'front 1';
    const back = 'back 1';
    const checked = true;

    const stateBefore = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 12345,
          streak: 2,
        },
      },
      allIds: ['0'],
    };
    const action = {
      type: actions.EDIT_CARD,
      id,
      front,
      back,
      checked,
    };
    const stateAfter = reducer(stateBefore, action);
    const { allIds } = stateAfter;
    const card = stateAfter.byId['0'];

    expect(allIds).toEqual(['0']);
    expect(card.front).toEqual('front 1');
    expect(card.back).toEqual('back 1');
    expect(card.review).toBeCloseTo(Date.now(), -2);
    expect(card.streak).toEqual(0);
  });

  it('should handle EDIT_CARD with unchecked', () => {
    const id = '0';
    const front = 'front 1';
    const back = 'back 1';
    const checked = false;

    const stateBefore = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 12345,
          streak: 2,
        },
      },
      allIds: ['0'],
    };
    const action = {
      type: actions.EDIT_CARD,
      id,
      front,
      back,
      checked,
    };
    const stateAfter = reducer(stateBefore, action);
    const { allIds } = stateAfter;
    const card = stateAfter.byId['0'];

    expect(allIds).toEqual(['0']);
    expect(card.front).toEqual('front 1');
    expect(card.back).toEqual('back 1');
    expect(card.review).toEqual(12345);
    expect(card.streak).toEqual(2);
  });

  it('should handle LOAD_CARDS_STATE', () => {
    const state = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 1234,
          streak: 0,
        },
        1: {
          id: '1',
          front: 'front 1',
          back: 'back 1',
          review: 1234,
          streak: 2,
        }
      },
      allIds: ['0', '1'],
    };
    const stateBefore = {
      byId: {},
      allIds: [],
    };
    const action = {
      type: actions.LOAD_CARDS_STATE,
      state,
    };
    const stateAfter = state;

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('should handle REMOVE_CARD', () => {
    const id = '0';
    const stateBefore = {
      byId: {
        0: {
          id: '0',
          front: 'front 0',
          back: 'back 0',
          review: 1234,
          streak: 0,
        },
        1: {
          id: '1',
          front: 'front 1',
          back: 'back 1',
          review: 1234,
          streak: 2,
        }
      },
      allIds: ['0', '1'],
    };
    const action = {
      type: actions.REMOVE_CARD,
      id,
    };
    const stateAfter = {
      byId: {
        1: {
          id: '1',
          front: 'front 1',
          back: 'back 1',
          review: 1234,
          streak: 2,
        },
      },
      allIds: ['1'],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});
