/* global describe expect it */

import reducer from '../../app/reducers/decks';
import * as actions from '../../app/actions/deck';

describe('decks reducer', () => {
  it('should handle initial state', () => {
    const stateBefore = undefined;
    const action = {};
    const stateAfter = {
      byId: {},
      allIds: [],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('should handle ADD_CARD_TO_DECK action', () => {
    const deckId = '0';
    const cardId = '0';

    const stateBefore = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: [],
        }
      },
      allIds: ['0'],
    };
    const action = actions.addCardToDeck(deckId, cardId);
    const stateAfter = reducer(stateBefore, action);

    expect(stateAfter).toEqual({
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: ['0'],
        },
      },
      allIds: ['0'],
    });
  });

  it('should handle CHANGE_DECK_NAME', () => {
    const stateBefore = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 1',
          cards: []
        },
        1: {
          id: '1',
          name: 'Deck 1',
          cards: []
        },
      },
      allIds: ['0', '1'],
    };
    const action = {
      type: actions.CHANGE_DECK_NAME,
      id: '0',
      newName: 'Deck 0',
    };
    const stateAfter = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: []
        },
        1: {
          id: '1',
          name: 'Deck 1',
          cards: []
        },
      },
      allIds: ['0', '1'],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('should handle CREATE_DECK action', () => {
    const name = 'Deck 1';
    const id = '1';
    const stateBefore = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: [],
        }
      },
      allIds: ['0'],
    };
    const action = actions.createDeck(name, id);
    const stateAfter = reducer(stateBefore, action);

    expect(stateAfter.byId).toHaveProperty('0');
    expect(stateAfter.byId['0']).toEqual({
      id: '0',
      name: 'Deck 0',
      cards: [],
    });
    expect(stateAfter.byId).toHaveProperty(id);
    expect(stateAfter.byId[id]).toEqual({
      id,
      name,
      cards: [],
    });
    expect(stateAfter.allIds).toHaveLength(2);
  });

  it('should handle LOAD_DECKS_STATE', () => {
    const state = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: []
        },
        1: {
          id: '1',
          name: 'Deck 1',
          cards: []
        }
      },
      allIds: ['0', '1'],
    };
    const stateBefore = {
      byId: {},
      allIds: [],
    };
    const action = {
      type: actions.LOAD_DECKS_STATE,
      state,
    };
    const stateAfter = state;

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('should handle REMOVE_CARD_FROM_DECK', () => {
    const deckId = '0';
    const cardId = '0';
    const stateBefore = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: ['0', '1'],
        },
        1: {
          id: '1',
          name: 'Deck 1',
          cards: [],
        },
      },
      allIds: ['0', '1'],
    };
    const action = {
      type: actions.REMOVE_CARD_FROM_DECK,
      deckId,
      cardId,
    };
    const stateAfter = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: ['1'],
        },
        1: {
          id: '1',
          name: 'Deck 1',
          cards: [],
        },
      },
      allIds: ['0', '1'],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });

  it('should handle REMOVE_DECK action', () => {
    const id = '0';
    const stateBefore = {
      byId: {
        0: {
          id: '0',
          name: 'Deck 0',
          cards: []
        },
        1: {
          id: '1',
          name: 'Deck 1',
          cards: []
        }
      },
      allIds: ['0', '1'],
    };
    const action = {
      type: actions.REMOVE_DECK,
      id,
    };
    const stateAfter = {
      byId: {
        1: {
          id: '1',
          name: 'Deck 1',
          cards: []
        }
      },
      allIds: ['1'],
    };

    expect(reducer(stateBefore, action)).toEqual(stateAfter);
  });
});
